# Name
## Salary service

# Description
This service allows you to receive data on the employee's salary and the date of the next promotion. A system of passwords and temporary tokens is used for authentication

# Installation
You can use a dockerfile to form the container or build the project locally using poetry.
## To build locally, install poetry
### Windows
```powershell
> (Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
 ```
### Linux, WSL, MacOS
```bash
> curl -sSL https://install.python-poetry.org | python3 -
```
And install dependencies
```bash
> cd salary_service
> poetry install --no-dev
```

# Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.
To use this service, there are 3 requests
## register - used to register a new employee 
Data format
```json
{
    "username": "newuser",
    "password": "password",
    "salary": 5000,
    "next_promotion": "yyyy-mm-dd"
} 
```
## login - used to login and get token
Data format
```json
{
    "username": "newusername",
    "password": "newpassword"

}
```
Request return
```json
{
    "access_token": "token",
    "token_type": "bearer"
}
```
## salary - return information about salary and next promotion date
Data format
```json
{
    "salary": 5000,
    "next_promotion": "yyyy-mm-dd"
}
```