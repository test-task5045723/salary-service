from fastapi.testclient import TestClient
from main import app, salary_data, get_password_hash, verify_password, create_access_token, decode_access_token, UserCredentials, UserRegistration, SalaryInfo
from datetime import datetime, timedelta
import pytest

client = TestClient(app)

# Test password hashing and verification
def test_password_hashing():
    password = "testpassword"
    hashed_password = get_password_hash(password)
    assert verify_password(password, hashed_password)

# Test JWT token creation and decoding
def test_jwt_token():
    data = {"sub": "testuser"}
    expires_delta = timedelta(minutes=30)
    token = create_access_token(data, expires_delta)
    assert decode_access_token(token) == "testuser"

# Test user authentication
def test_user_authentication():
    credentials = UserCredentials(username="testuser", password="testpassword")
    response = client.post("/login", json=credentials.dict())
    assert response.status_code == 200
    assert "access_token" in response.json()

# Test user registration
def test_user_registration():
    user = UserRegistration(username="newuser", password="newpassword", salary=50000, next_promotion="2022-01-01")
    response = client.post("/register", json=user.dict())
    assert response.status_code == 200
    assert response.json()["message"] == "User registered successfully"
    assert "newuser" in salary_data

# Test getting salary information
def test_get_salary():
    credentials = UserCredentials(username="testuser", password="testpassword")
    response = client.post("/login", json=credentials.dict())
    assert response.status_code == 200
    access_token = response.json()["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}
    response = client.get("/salary", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"salary": 50000, "next_promotion": "2022-01-01"}

# Test invalid authentication credentials
def test_invalid_credentials():
    credentials = UserCredentials(username="testuser", password="wrongpassword")
    response = client.post("/login", json=credentials.dict())
    assert response.status_code == 401
    assert response.json()["detail"] == "Invalid username or password"

# Test invalid JWT token
def test_invalid_token():
    headers = {"Authorization": "Bearer invalidtoken"}
    response = client.get("/salary", headers=headers)
    assert response.status_code == 401
    assert response.json()["detail"] == "Invalid authentication credentials"

# Test salary information not found
def test_salary_not_found():
    credentials = UserCredentials(username="nonexistentuser", password="testpassword")
    response = client.post("/login", json=credentials.dict())
    assert response.status_code == 200
    access_token = response.json()["access_token"]
    headers = {"Authorization": f"Bearer {access_token}"}
    response = client.get("/salary", headers=headers)
    assert response.status_code == 404
    assert response.json()["detail"] == "Salary information not found"