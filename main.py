from fastapi import FastAPI, HTTPException, Depends
from datetime import datetime, timedelta
from pydantic import BaseModel
from passlib.context import CryptContext
from jose import JWTError, jwt

# Конфигурация
SECRET_KEY = "your-secret-key"  # Замените на свой секретный ключ
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

# Пример данных о зарплате и дате следующего повышения
salary_data = {}

# Модель данных для аутентификации
class UserCredentials(BaseModel):
    username: str
    password: str

# Модель данных для регистрации нового пользователя
class UserRegistration(BaseModel):
    username: str
    password: str
    salary: int
    next_promotion: str

# Модель данных для ответа с информацией о зарплате
class SalaryInfo(BaseModel):
    salary: int
    next_promotion: str

app = FastAPI()
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# Генерация хэша пароля
def get_password_hash(password: str):
    return pwd_context.hash(password)

# Проверка пароля
def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

# Создание JWT-токена
def create_access_token(data: dict, expires_delta: timedelta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

# Проверка валидности JWT-токена
def decode_access_token(token: str):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username = payload.get("sub")
        if username is None:
            raise HTTPException(status_code=401, detail="Invalid authentication credentials")
        return username
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid authentication credentials")

# Аутентификация пользователя
def authenticate_user(credentials: UserCredentials):
    username = credentials.username
    password = credentials.password
    if username not in salary_data:
        raise HTTPException(status_code=401, detail="Invalid username or password")
    hashed_password = get_password_hash(password)
    if not verify_password(password, hashed_password):
        raise HTTPException(status_code=401, detail="Invalid username or password")
    return username

# Проверка наличия валидного JWT-токена
def get_current_username(token: str = Depends(decode_access_token)):
    return token

# Маршрут для аутентификации и получения JWT-токена
@app.post("/login")
def login(credentials: UserCredentials):
    username = authenticate_user(credentials)
    access_token = create_access_token(data={"sub": username}, expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES))
    return {"access_token": access_token, "token_type": "bearer"}

# Маршрут для регистрации нового пользователя
@app.post("/register")
def register_user(user: UserRegistration):
    username = user.username
    password = user.password
    salary = user.salary
    next_promotion = user.next_promotion

    if username in salary_data:
        raise HTTPException(status_code=400, detail="User already exists")

    hashed_password = get_password_hash(password)
    salary_data[username] = {"salary": salary, "next_promotion": next_promotion}

    return {"message": "User registered successfully"}

# Маршрут для получения информации о зарплате
@app.get("/salary")
def get_salary(current_username: str = Depends(get_current_username)):
    salary_info = salary_data.get(current_username)
    if not salary_info:
        raise HTTPException(status_code=404, detail="Salary information not found")
    return SalaryInfo(salary=salary_info["salary"], next_promotion=salary_info["next_promotion"])
