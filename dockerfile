# Используем базовый образ Python
FROM python:3.11

# Устанавливаем переменную окружения для отключения буферизации вывода
ENV PYTHONUNBUFFERED 1

# Создаем директорию контейнера для приложения
RUN mkdir /app

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Копируем файлы зависимостей Poetry
COPY pyproject.toml poetry.lock /app/

# Устанавливаем Poetry
RUN pip install poetry

# Устанавливаем зависимости Poetry
RUN poetry install --no-dev

# Копируем остальные файлы проекта в контейнер
COPY . /app

# Запускаем сервер FastAPI
CMD ["poetry", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
